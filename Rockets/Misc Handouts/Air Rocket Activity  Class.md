# Air Rocket Activity / Class

## Intro

- provide overview of class
- demo materials
- how does an air rocket work
- 

## Discuss ballistic flight

- Ballistic flight vs powered flight
- simple equations
- things that affect flight
- desiging a rocket
- show images of different rocket designs from V2 to SpaceX
- calculating height of flight (angle and distance) from observations
- calculating heigh from flight using math
- What is drag
- show graphs on how variables affect flight of the rocket
- Invite older age group for a more indepth physics discussion

## Building an Air Rocket

- Review templates
- Self design vs using a template
- Discuss materials and instructions
- Distribute materials and begin assembling rockets
- pick up clean up
  - put away materials and trash
- take simple measurements before heading outside. mass, length, etc

## Flying Rockets

- discuss safety rules
  - Don't Catch falling rockets
  - We will need observers/spotters
  - Safety Zone
  - Eye protection
  - Count downs
- Data Recording- show data sheet
  - time of flight
  - pressure
  - observed angle @ 100ft
  - Weight
  - Launch angle?
- Rotate jobs
  - launcher
  - spotter
  - 

## Conclusion

- What did you notice about rocket flight?
- What affect do fin size, shape, number and placement have on flight?
- What affect does weight have on rocket flight?
- What would you change to make the class / activity more fun?
- If you were to redesign your rocket, what changes would you make?