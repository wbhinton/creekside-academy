\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Objective}{3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Description}{3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Materials}{3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Rocket Design}{3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Weight}{3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Aerodynamics}{7}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Flight Equations}{9}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Air Rockets vs Continuous Thrust Rockets}{9}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Ballistic Flight}{10}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3}Flight Equations}{10}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.3.1}Air Rocket Launch Equations}{10}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.3.2}Ballistic Flight Equations}{10}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.3.3}Flight Equations with Drag}{12}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4}Calculating the Height Using Trigonometry}{13}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Procedure}{13}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Discussion}{15}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}Assessment}{15}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9}Safety Plan}{15}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.1}Rocket Launch Site}{16}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.2}Rocket Launcher Setup}{16}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.3}Launch Crew}{16}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.4}Launch and Recovery}{16}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {10}Data Recording Sheets}{16}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {11}Bonus: Python Code}{20}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{Glossary}{21}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{References}{22}%
