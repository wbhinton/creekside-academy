\documentclass[12pt,letterpaper,leqno]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{pdfpages}
\usepackage[backend=bibtex]{biblatex}
%Imports biblatex package
\addbibresource{resources.bib} %Import the bibliography file
\usepackage[toc,nonumberlist]{glossaries}


\usepackage{float}
\usepackage{placeins}
\usepackage[margin=0.75in]{geometry}
\renewcommand{\familydefault}{\sfdefault}


\author{Weston Hinton}
\title{Compressed Air Rockets}

\makeglossaries

\newglossaryentry{vel}{name={velocity},description={the speed of something in a given direction}}

\newglossaryentry{ball}{name={ballistic flight},description={the path of an unpowered object, as a missile, moving only under the influence of gravity and possibly atmospheric friction and with its surface providing no significant lift to alter the course of flight.}}

\newglossaryentry{gf}{name={g-force},description={the force of gravity or acceleration on a body}}
	

\newglossaryentry{drag}{name={drag},description={the force acting opposite to the relative motion of any object moving through the air}}

\newglossaryentry{aero}{name={aerodynamics},description={properties of moving air and the interaction between the air and solid bodies moving through it.}}

\newglossaryentry{ac}{name={acceleration},description={The rate of change of velocity with respect to time.}}

\newglossaryentry{cog}{name={center of gravity},description={The point in or near a body at which the gravitational potential energy of the body is equal to that of a single particle of the same mass located at that point and through which the resultant of the gravitational forces on the component particles of the body acts.}}

\newglossaryentry{ballast}{name={ballast},description={Heavy material that is placed in the hold of a ship to enhance stability}}

\newglossaryentry{fuse}{name={fuselage},description={The central body of an aircraft, to which the wings and tail assembly are attached and which accommodates the crew, passengers, and cargo}}

\newglossaryentry{dc}{name={drag coefficient},description={the ratio of the drag on a body moving through air to the product of the velocity and the surface area of the body}}

\newglossaryentry{lv}{name={launch velocity},description={the velocity of the rocket immediately as it leaves the launch tube.}}

\newglossaryentry{thr}{name={thrust},description={To push or drive quickly and forcefully: synonym: push.}}

\newglossaryentry{f}{name={force},description={The capacity to do work or cause physical change; energy, strength, or active power.}}

\newglossaryentry{TLO}{name={TLO},description={Lift off time, the time it takes for the rocket to leave the launch tube.}}

\newglossaryentry{g}{name={gravitational constant},description={An empiricalphysical constant involved in the calculation of the gravitational attraction between objects with mass. }}

\newglossaryentry{apex}{name={apex},description={The highest point; the vertex}}

\newglossaryentry{tan}{name={tangent},description={The trigonometric function of an acute angle in a right triangle that is the ratio of the length of the side opposite the angle to the length of the side adjacent to the angle.}}

\newglossaryentry{ltrig}{name={trigonometry},description={The branch of mathematics that deals with the relationships between the sides and the angles of triangles and the calculations based on them, particularly the trigonometric functions.}}

\newglossaryentry{tv}{name={terminal velocity},description={The speed at which an object in free-fall and not in a vacuum ceases to accelerate downwards because the force of gravity is equal and opposite to the drag force acting against it.}}


\begin{document}
	
	

	\glsaddall
	
	\begin{center}
		\vspace*{1cm}
		
		\Huge
		\textbf{Compressed Air Rockets}
		
	
		

		
		\vspace{0.8cm}
		
		\includegraphics[width=0.4\textwidth]{logo1.png}
		
		\Large
		Franklin Homeschool Meetup\\
		Macon County Public Library\\
		October, 7 2021
		
	\end{center}
\newpage


\tableofcontents
\newpage
\nocite{*}
\section{Objective}
Design, construct, and launch compressed air rockets. Record data and evaluate rocket flight. Modify rocket design to improve flight performance. 

\section{Description}
Students will receive a brief overview of rockets, rocket design, \textbf{ballistic flight} and how to build an air rocket. Everyone will then be provided materials and instructions to construct an air rocket. Once everyone has had the chance to build their rockets, we will proceed outside to launch our rockets (weather permitting). Students will record data and observations on their rocket data sheet and mission report sheet.

\section{Materials}
\begin{itemize}
	\item AirRocketWorks.com compressed air rocket launcher
	\item High pressure bicycle pump 
	\item Heavy weight (24lb) printer paper or card stock
	\item Index cards
	\item \textit{Optional} AirRocketWorks.com paper rocket template (provided as a separate handout)
	\item Scissors
	\item Tape (cellophane, masking, packing, etc.)
	\item Fuselage form tube ($\frac{1}{2}$in PVC or PEX 12in long)
	\item Kitchen Scale 
	\item Ruler
	\item Eye Protection (for the launch crew)

	
	\end{itemize}


\section{Rocket Design}
\subsection{Weight}
When building your rocket, one of the variables you can control is the weight or mass of your rocket. How much your rocket weighs will affect how fast and high it will fly. Imagine you have 3 balls; a wiffle ball, baseball, and cannonball. Which one could you throw the farthest? Obviously the baseball will travel the farthest, the wiffle ball is too light and the cannonball is too heavy to be thrown very far. Similar to this example, there will be an ideal weight for your rocket if you want to maximize its performance.  Too light or too heavy and it won't fly very high.
\newline

Below are some plots illustrating how the rocket's mass impacts different aspects of the flight. Based on the launch parameters shown in Table \ref{t:1}.

\begin{table}[!htb]\centering
	\begin{tabular}{l|c}
		Parameter & Value \\ \hline
		Length & 11 in \\
		Diameter & .852 in \\
		Mass & 25 g \\
		Launch Pressure & 130 psi \\
		Drag & .7 
	\end{tabular}
\caption{Rocket Parameters}
\label{t:1}
\end{table}
    \FloatBarrier


 Figure \ref{fig:1} shows you how high the rocket will fly depending on how heavy it is. The horizontal axis is the rocket mass and the vertical axis is the height of the rocket. The solid line shows you how the height of the rocket's flight changes as the mass of the rocket changes. The dotted line shows the 25 gram mass of our example rocket would be. 
 
 \begin{quote}
 	Looking at Figure \ref{fig:1}, What do you think is the \underline{ideal} weight of the rocket?
 \end{quote}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=\linewidth]{massVheight.png}
	\caption[Effect of Mass on Height]{The effect of changing the rocket's mass on the maximum height}
	\label{fig:1}

	
	\end{figure}
    \FloatBarrier

Figure \ref{fig:2} shows you how the \textbf{velocity} of the rocket at launch changes depending on how heavy it is. The horizontal axis is the rocket mass and the vertical axis is the \textbf{launch velocity} in miles per hour. The solid line shows you how the launch velocity changes as the rocket mass is increased or decreased. The dotted line shows the 25 gram mass of our example rocket. 

\begin{quote}
	Looking at Figure \ref{fig:2}, What is happening to the launch velocity as the rocket gets heavier?
\end{quote}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=\linewidth]{massVvelocity.png}
	\caption[Mass vs Velocity]{The effect of changing the rocket's mass on the launch velocity}
	\label{fig:2}

	
\end{figure}
    \FloatBarrier

Figure \ref{fig:3} shows you how total flight time  depending on how heavy it is. The horizontal axis is the rocket mass and the vertical axis is total flight time in seconds. The solid line shows you how the flight time changes as the rocket mass is increased or decreased. The dotted line shows the 25 gram mass of our example rocket. 

\begin{quote}
	Looking at Figure \ref{fig:3}, Why do you think the light weight rockets have low total flight times?
\end{quote}

\begin{figure}[!htb
	]\centering
	\includegraphics[width=\linewidth]{massVflighttime.png}
	\caption[Effect of Mass on Flight Time]{The effect of changing the rocket's mass on the total flight time of the rocket}
	\label{fig:3}

	
	
\end{figure}
    \FloatBarrier

Figure \ref{fig:4} shows you how the \textbf{g-force} exerted on the rocket at launch changes depending on how heavy it is. The horizontal axis is the rocket mass and the vertical axis is the g-force measured in G's. The solid line shows you how the g-force changes as the rocket mass is increased or decreased. The dotted line shows the 25 gram mass of our example rocket. The average person can withstand about 5 G's before losing consciousness. The highest peak G's that a human has survived during tests was 42.6 by John Stapp in 1954 during rocket engine testing. 

\begin{quote}
	Looking at Figure \ref{fig:4}, Why are the G's so much higher for the light weight rockets?
\end{quote}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=\linewidth]{massVgs.png}
	\caption[Effect of Mass on G-force]{The effect of changing the rocket's mass on the G-Force at launch}
	\label{fig:4}

	
\end{figure}
    \FloatBarrier
    
    
    
    \subsection{Aerodynamics}
    
    Have you ever stuck your hand out of the window of a car when its going fast? That push on your hand is air pushing back as your hand moves through it. You may have also noticed that depending on how you hold your hand the air might push it up or down, hard or not as hard. The scientific term for this pushing force is \textbf{drag}. When you are building your rocket, you want to minimize the drag force. Below are some plots showing how drag impacts the flight of your rocket. According to NASA, a  typical paper air rocket will have a \textbf{drag coefficient} of 0.7. 
    
    
    Figure \ref{fig:5} shows you how high the rocket will fly depending on the drag coefficient. 
    
    
    \begin{figure}[!htb]
    	\centering
    	\includegraphics[width=\linewidth]{dragVheight.png}
    	\caption[Effect of Drag on Height]{The effect of changing the rocket's drag coefficient on the maximum height}
    	\label{fig:5}
    	
    	
    \end{figure}
    \FloatBarrier
    
    \textbf{Terminal velocity} is how fast an object can fall through the air. For example a feather has a lower terminal velocity than a hammer. Its pretty easy to observe this, drop a hammer and feather from the same height. Which one hits the ground first? Obviously the hammer hits first, it is falling to the ground faster, it has a higher terminal velocity. If there is no air, the terminal velocity would be the same for the feather and hammer. NASA astronauts demonstrated this very principle by dropping a feather and hammer on the moon, both hit the ground at the exact same time.
    
    \begin{figure}[!htb]
    	\centering
    	\includegraphics[width=\linewidth]{dragVterminalvel.png}
    	\caption[Effect of Drag on Terminal Velocity]{The effect of changing the rocket's drag coefficient on the terminal velocity}
    	\label{fig:6}
    	
    \end{figure}
    \FloatBarrier
    
    
\section{Flight Equations}
These equations can be tricky and in some cases there needs to be some unit conversions to make everything work properly. There is a Jupyter Lab notebook that walks you through the equations, check the Bonus Section for more information.
\subsection{Air Rockets vs Continuous Thrust Rockets}
An air rocket is propelled by a single strong burst of air that blasts the rocket into the sky. After that burst of air, there are no more forces pushing the rocket up. Rockets that send satellites and astronauts into space have engines that provide \textbf{thrust} or \textbf{force} for their entire flight. The continuous force from the engines help send these rockets much higher than the air rockets we will build today.  

\subsection{Ballistic Flight}
Air rockets are objects that are in \textbf{ballistic flight}, meaning they do not fly through the air under their own power. Other examples of ballistic flight are: arrows, thrown or hit balls and bullets. When an object is in ballistic flight we can use math to predict how it will fly through the air. The following section presents and discusses the equations used to calculate how the air rockets will fly based on our measurements.

\subsection{Flight Equations}
\subsubsection{Air Rocket Launch Equations}
These launch equations describe how the rocket is moving during lift off. They also provide data that will be used later to calculate how high the rockets can fly. 
\newline
\newline
$p_a =$ atmospheric pressure, psi \\
$p_g =$ gauge pressure on the pump, psi\\
$A= $ cross sectional area of the rocket\\
$L = $ length in inches\\
$v = $ velocity\\
$a = $ acceleration\\
$g= $ gravitational constant 32.174$\frac{ft}{s^2}$\\
$t= $ time \\
$TLO= $ lift off time, the time it takes the rocket to leave the launch tube\\
$w= $ weight in grams\\
\newline
\newline
Calculate the acceleration of the rocket using the launch pressure, cross sectional area and weight.
\begin{equation}
	a= g [\frac{(p_g-p_a) A}{w}-1]
\end{equation}
Calculate the lift off time (\textbf{TLO}) using the rocket length and acceleration.
\begin{equation}
	TLO =  \sqrt{\frac{2L}{a}}
\end{equation}
Calculate the initial or launch velocity of the rocket using TLO and acceleration. Or you can calculate the velocity using the rocket length and acceleration. 
\begin{equation}
	V = a TLO
\end{equation}
\begin{equation}
	V = \sqrt{2 L a}
\end{equation}
\subsubsection{Ballistic Flight Equations}
These equations describe the flight of the air rocket if there were no drag forces acting on it. These equations are simpler to understand than the flight equations with drag, but also do not provide as accurate of an estimate of the rocket's flight. These equations will over estimate how high the rocket will fly, since they do not account wind resistance.
\newline
\newline
$t =$ time, s\\
$y= $ height. ft\\
$V = $ vertical velocity, fps\\
$V_0 = $ initial velocity, fps\\
$g= $ gravitational constant\\
\newline
\newline
\textbf{For a vertical launch}
\newline
Calculate the vertical velocity at any time $t$, time is the only variable, as we previously calculated the initial rocket velocity and we know that g is 32.174 
\begin{equation}
	V = V_0 - gt
\end{equation}
Calculate the height of the rocket at any time $t$, again time is the only variable in this equation.
\begin{equation}
	y = V_0t - .5 g t^2
\end{equation}
\textbf{Apex}
\newline
At the highest point the rocket will fly, we know that it slows down to a velocity of 0 before it begins to fall back to earth. 
\begin{equation}
	V = 0
\end{equation}
Calculate the time it takes to reach the max height using the initial velocity and gravity.
\begin{equation}
	t = \frac{V_0}{g}
\end{equation}
Calculate the max height reached by the rocket using the initial velocity and gravity.
\begin{equation}
	y = \frac{.5V^2_0}{g}
\end{equation}
\textbf{At Ground Impact}
\newline
When the rocket reaches the ground, we know that the height will be equal to 0. 
\begin{equation}
	y = 0
\end{equation}
If there is no drag, we can assume fall to the ground in the same amount of time it took to reach it's max height. So the time from launch until the rocket hits the ground is:
\begin{equation}
	t = 2 \frac{V_0}{g}
\end{equation}
Since there is no drag, the rocket will be going just as fast when it hits the ground as it was at launch. It's just going down instead of up... so its negative. 
\begin{equation}
	V = -V_0
\end{equation}
\subsubsection{Flight Equations with Drag}
The following equations take into consideration the drag force experienced by the rocket on both the ascent and descent portions of the flight. Drag is the force exerted by the air on the rocket as it tries to move through it. 
\newline
\textbf{Coasting Ascent}
\newline
The force is mass time acceleration. The net force on the rocket on it's ascent is the negative force due to gravity minus the force due to drag.\\
$F_{net} =$ net force acting on the rocket\\
$F_g =$ gravitation force on the rocket\\
$F_d =$ drag force acting on the rocket\\
$a_d =$ acceleration of the rocket accounting for drag\\
$V_{term} =$ terminal velocity\\
$y =$ height of rocket at any time $t$\\
$t =$ time in seconds after launch\\
$C_d =$ drag coefficient\\
$m =$ mass\\
$g =$ gravitational constant\\

\begin{equation}
	F_{net} = -F_g- F_d
\end{equation}
Before you can calculate the net force, you need to calculate the acceleration due to drag. $C_d$ is the coefficient of drag, and according to NASA, a typical paper air rocket with have a $C_d$ of 0.7
\begin{equation}
	a_d = -g - \frac{C_d r A V^2}{2m}
\end{equation}
Calculate the \textbf{terminal velocity} of your rocket. Terminal velocity is how fast it can fall with the air pushing back on it. 
\begin{equation}
	V_{term} = \sqrt{\frac{2mg}{Cd* r A}}
\end{equation}
Using Terminal Velocity and Initial Velocity, you can calculate the velocity of the rocket at any time $t$. 
\begin{equation}
	V = V_{term} \frac{V_0 - V_{term} tan( t \frac{g}{V_{term}})}{V_{term} + V_0 tan( t )}
\end{equation}
Calculate the height of the rocket at any time $t$.
\begin{equation}
	y = \frac{V^2_{term}}{2g} ln(\frac{V^2_0+V^2_{term}}{V^2+V^2_{term}})
\end{equation}
\textbf{Apex}
\newline
Calculate the maximum height the rocket can fly with drag.
\begin{equation}
	y_{max} = \frac{V^2_{term}}{2g} ln(\frac{V^2_0+V^2_{term}}{V^2_{term}})
\end{equation}
Calculate the time it takes to reach the max height.
\begin{equation}
	t = \frac{V_{term}}{2g} tan^{-1}(\frac{V_0}{V_{term}})
\end{equation}
\subsection{Calculating the Height Using Trigonometry}
You can also calculate how high your rocket traveled using triangles and some simple \textbf{trigonometry}. You will need to know the distance between your observation location and the launch site. From your observation site you will observe and measure the angle between you and the peak of the rockets flight.
\newline
$h=$ height, ft\\
$d= $ distance from launch site, ft\\
$\theta= $ angle \\
\begin{equation}
	h = d * tan(\theta)
\end{equation}



\section{Procedure}
Below are written and illustrated procedures for building a rocket. The illustrated instructions are for a simplified version. Templates from AirRocketWorks.com are also available, these have their own set of instructions printed on the template itself. There will also be a demonstration on how to build the air rocket.
\begin{enumerate}
	\item Draw a quick sketch of what you want your rocket to look like. Draw the body, nose cone and fins. 
	\item Roll a tube of paper around the support pipe and tape the seam. You can use either a whole sheet of paper or a half sheet. 
	\item Crimp the one end of the rocket by sliding about half of an inch of paper tube beyond the end of the support pipe and fold it in on itself. Apply a small amount of tape over the crimped end so it doesn't come apart from the launch pressure.
	\item Cut out and curl a nose cone, adjust it to fit the crimped end of the rocket. Then tape the seam of the nose cone.
	\item Add \textbf{ballast} if desired to the nose cone section. Ballast will add weight to the rocket and shift the \textbf{center of gravity} more towards the nose of the rocket. (stuff crumpled scraps of paper into the nose cone, and / or use a small washer )
	\item Attach the nose cone to the crimped end of the rocket using tape. 
	\item If desired, wrap the rocket with tape for extra strength. It will add some weight to the rocket, but it may prevent a blowout at launch.
	\item Cut out your fins from an index card, or paper. 
	\item Carefully attach your fins using tape to the rocket. Try to space them as evenly as possible.
	\item Prepare for launch. 
	\item Measure the length of your rocket and use the scale to weigh your rocket. Record the data on your data sheet. 
	\item While waiting for your turn to launch, look at some of the other rockets that have been built. 
	
\end{enumerate}
\includepdf[pages=-]{basicairrocket.pdf}

\section{Discussion}

\begin{itemize}
	\item How can air rockets be modified to improve their performance?
	\subitem There are several possible adjustments to the rocket design that can be made. The size and shape of the fins. Placement and quantity of the fins. Fins that are mounted straight or at an angle. Nose cone shape. Nose cone weight. Total rocket weight. Rocket length. Rocket diameter. How tight or loose the rocket is on the launch tube.
	\item Is it ok to change more than one thing when trying to improve the rocket design?
	\subitem Yes, but if you change more than one thing on a new rocket design, you won't have any way to determine what made the biggest difference in the new flight results.
	\item How heavy should the rocket be?
	\subitem Look back at the graphs in the packet to get an idea of the ideal weight. 
	\item How many fins are needed?
	\subitem 0, 1, 2, 3, 4, 5??
	\item How will the size and shape of the fins affect the flight?
	\item Should the fins be placed near the bottom of the rocket or near the top? Or both?
	\item Will the smoothness of the rocket body change it's flight?
	\item Does the nose cone shape change the way the rocket flies?
\end{itemize}
\section{Assessment}
\begin{itemize}
	\item Review the student mission reports, data sheets and conclusions. Discuss the findings with them.
	\item Have students write a paper discussing what they have learned about rockets and rocket flight during this activity. 
	\item Invite students to redesign and build new rockets to test out ideas based on what they have learned. 
	\item For older students have them calculate how fast or how high their rocket should go using the flight equations included in this packet. 
\end{itemize}

\section{Safety Plan}
If anyone feels unsafe at anytime during the launch process, they should bring it to the attention of the Range Safety Officer or any other leader of the event. We will stop and discuss the concerns before moving forward. 
\subsection{Rocket Launch Site}

\begin{itemize}
	\item Launch rockets outside if using high pressures. A large indoor space such as a gymnasium could be used, but keep pressures below 50psi to prevent impacting the ceiling.
	\item Select an open area free from obstructions (no power lines, trees, buildings, etc.) Below are some examples of good locations
	\subitem- soccer field, parks, or other open fields
	\item Use something to mark off a launch zone and spectator zone. Stakes and flagging tape or sports cones work well. 
	
\end{itemize}
\subsection{Rocket Launcher Setup}
\begin{itemize}
	\item Place launcher on a flat level ground
	\item Aim the launch tube at a slight angle away from the launch crew and spotters. This will help prevent the rockets from landing on anyone.
	\item If there is any wind, aim the launch tube accordingly to prevent rockets from straying into people or objects.
\end{itemize}
\subsection{Launch Crew}
\begin{itemize}
	\item Move all observers and spotters back into the safe zone (10-12ft)
	\item All rocketeers involved in the launch must wear eye protection. 
	\item Limit pressure of the launcher to under 130psi (the safety valve should pop if you go over 130psi)
	\item When given the all clear from the Range Safety Office, begin the countdown and launch the rocket.
\end{itemize}
\subsection{Launch and Recovery}
\begin{itemize}
	\item Maintain spotters at some distance away from the launch area to keep visual contact on the rocket throughout the flight. 
	\item Do not try to catch the rockets. They are coming down fast and will hurt if they hit you.
	\item Retrieve the rocket before launching the next one.
	\item Do not launch if anyone or anything that could be damaged by falling rockets is in the landing zone.
\end{itemize}


\section{Data Recording Sheets}
\begin{itemize}
	\item Air Rocket Data Sheet
	\item Air Rocket Mission Report
	\item Air Rocket Height Worksheet
\end{itemize}
\newpage
\includepdf[pages=-]{datasheetsvg.pdf}
\includepdf[pages=-]{missionreport.pdf}
\includepdf[pages=-]{heightworksheet.pdf}

\section{Bonus: Python Code}

If you are looking for an extra challenge, check out some Python code that will let you evaluate the flight equations for different rocket parameters. You can play around with different weights, launch pressures, diameters, lengths, drag coefficients to see how it affects rocket velocity and height.




The code is in an interactive Jupyter Notebook hosted on Google Colab. You will need a Google Account if you want to execute any of the code and see how the variables change the results.  You don't have to install anything, just follow the link\\
 \textbf{https://colab.research.google.com/drive/1QM11Zv7kD8GLREbye7SKM9IkLRd3K5f-?usp=sharing}\\ or scanning the QR Code below.\\
 \\
 \includegraphics[width=.25\linewidth]{colabqr.png}\\
 
 You can also download a copy of the notebook from the Google Colab to run on your computer. However you will need to install the Anaconda (\textbf{https://www.anaconda.com/products/individual}) to allow you run the Jupyter notebook.

\clearpage

\printglossary[title= Glossary, toctitle=Glossary]
\newpage

\nocite{*}
\printbibliography[heading=bibintoc]



\end{document}
