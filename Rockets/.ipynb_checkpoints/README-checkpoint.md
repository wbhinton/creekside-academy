# Rockets

This repo contains the resources and files related to compressed air rockets.

## Resources

- [Paper Rocket Template](https://airrocketworks.com/wp/wp-content/uploads/2017/05/8.5x11PaperRocketTemplate.pdf)
- [3 Fin Guide](https://www.airrocketworks.com/wp/wp-content/uploads/2017/05/ThreeFinGuide.pdf)
- [Flight Equations with Drag](https://www.grc.nasa.gov/www/k-12/rocket/flteqs.html)
- [Basic Ballistic Flight Equations](https://www.grc.nasa.gov/www/k-12/rocket/ballflght.html)
- [Ballistic Flight Calculator](https://www.grc.nasa.gov/www/k-12/rocket/fltcalc.html)
- [Air Rocket Works](https://www.airrocketworks.com/wp/)
- [Air Rocket Works- Thingiverse](https://www.thingiverse.com/airrocketworks/designs)
- [Air Rocket Works- Youmagine](https://www.youmagine.com/air-rocket-works-llc-keith/designs)
- [EAA Air Rockets PDF](https://www.eaa.org/en/eaa/eaa-chapters/eaa-chapter-resources/~/media/d8770d90abf34b77ae751c00ee4d22cf.ashx)
- [Civil Air Patrol- Air Rockets](https://www.gocivilairpatrol.com/media/cms/Newtons_Laws_as_they_apply_to_Rocke_7633BAE38586A.pdf)
- [Make- Basic CAR](https://makezine.com/2008/12/13/compressed-air-rocket/)
- [Make- CAR](https://makezine.com/projects/compressed-air-rocket/)
- [Air Rocket 8th Grade Lesson Plan](https://betterlesson.com/lesson/640589/air-rockets)
- [NASA CAR Activity Sheet](https://www.nasa.gov/pdf/544882main_E6_Ride-The-Wind_C3.pdf)
- [Northeaster University STEM CAR Lesson Plan](https://stem.northeastern.edu/programs/ayp/fieldtrips/activities/rockets/)
- [STEM 30](https://airandspace.si.edu/sites/default/files/iss-science-launch.pdf)
- [Discoverspace.org](https://www.discoverspace.org/wp-content/uploads/2020/06/Stomp-Rockets-Lesson.pdf)
- [Civil Air Patrol-2](https://files.eric.ed.gov/fulltext/ED477701.pdf)
- [4H Oregon State](https://extension.oregonstate.edu/sites/default/files/documents/10551/stemactivity-engineer-airrocketslesson.pdf)
- [SC Activities](http://scactivities.cikeys.com/rocket-launchers/for-teachers/)
- [Bioedonline.org](http://www.bioedonline.org/BioEd/cache/file/533F74F8-B262-CA70-FC66C23F260A12FB.pdf)
- [4H TAMU](https://texas4-h.tamu.edu/wp-content/uploads/2015/09/Advanced_High_Power_Paper_Rockets.pdf)
- [Flexcart Lesson Plans](https://stemflexcart.com/air-rocket.php)
- [Surviving a Teachers Salary](https://www.survivingateacherssalary.com/how-rockets-work-unit-study-lesson-for-kids-k-5th/)
- 
- [OpenRocket Simulator](https://openrocket.info/)