                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1729214
Project: Paper Air Rocket Tail Sections by insane66 is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

I have designed this project as an extension to the paper air rockets that were in MAKE Magazine Volume 15. I have designed a variety of tails and an activity to test what type of aerodynamics provide the best flight for these small inexpensive rockets.

I have a related nose cone project: http://www.thingiverse.com/thing:1729183
https://youtu.be/VPjsU4TNLrg

# Print Settings

Printer: Generic Prusa i3
Rafts: No
Supports: Yes
Resolution: 0.2mm
Infill: 100%

Notes: 
Printed with 1.2mm walls so effectively solid infill.

# How I Designed This

Took measurements of PVC pipe and sketched up a simple idea of how the paper interface would work and looked at a ruler for a minute using my engineering judgement to determine what I thought a good length for the main tube to attach the tail fins to.  Next I sketched out a couple ideas on tail fin style that I planned to model. 

I then moved into Fusion 360 and made concentric circles to represent the PVC and the paper ledge for the tail tube. I extruded these out to the length I determined earlier.  Next I moved to side profile and then started sketching profiles of 1 fin.  I used a 2 sided extrusion on the fin, to keep it centered and perpendicular, and created a new body.  Next,I grabbed that new fin body and created an array around the central axis with 3 or 4 copies as needed. 

The only changes to this were some modifications needed to improve the interface of the fins with the body, or to rotate the fins that are rotated (which also required extra extrusion to tie into the main body. 

Finally I combined all the bodies in the model before exporting the STL files. 

** TIP ** 
Make sure you do not have little bits of fin sticking down past the bottom of the tail body.  See the picture below for what happened to me. 



![Alt text](https://cdn.thingiverse.com/assets/79/ec/87/a6/d6/20160825_152731.jpg)
Little tiny triangles of fin where sticking down past the body and kept several of my tails from sticking to the build plate, which in turn destroyed the whole print!

# Project: Paper Air Rocket Tails

<iframe src="//www.youtube.com/embed/VPjsU4TNLrg" frameborder="0" allowfullscreen></iframe>

![Alt text](https://cdn.thingiverse.com/assets/1a/3a/29/0b/8e/20160828_134442.jpg)
Secure tail to paper and use PVC to roll the body.

![Alt text](https://cdn.thingiverse.com/assets/fa/c1/61/42/d3/20160828_135327.jpg)
Tape it all up and smooth it out

![Alt text](https://cdn.thingiverse.com/assets/7d/6d/fc/3d/9a/20160828_135849.jpg)
Launch setup from Make Magazine article with rocket ready to go!

![Alt text](https://cdn.thingiverse.com/assets/82/dc/ea/49/9a/20160828_140947.jpg)
Ready for launch in T-minus 3....2...

![Alt text](https://cdn.thingiverse.com/assets/9d/32/64/8e/5a/20160828_141354.jpg)
Can you say 

![Alt text](https://cdn.thingiverse.com/assets/b1/91/6e/4a/ec/20160828_141748.jpg)
Rocket on right straightened right back out.