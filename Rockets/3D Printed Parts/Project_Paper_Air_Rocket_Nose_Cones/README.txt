                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1729183
Project: Paper Air Rocket Nose Cones by insane66 is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

I have designed this project as an extension to the paper air rockets that were in  MAKE Magazine Volume 15. I have designed a variety of nose cones and an activity to test what type of aerodynamics provide the best flight for these small inexpensive rockets. 

I have a related tail project: http://www.thingiverse.com/thing:1729214
https://youtu.be/VPjsU4TNLrg

# Print Settings

Printer: Generic Prusa i3
Rafts: No
Supports: Yes
Resolution: 0.2mm
Infill: 40%

Notes: 
Printing with a soft rubbery TPE could be a good idea for safety (Although more expensive, and not supported by all printers).  Use caution when launching, results can get surprising when launching. 

# How I Designed This

I started out by measuring the PVC pipe and sketching up an interface for the paper body of the rocket.  I created some concentric circles representing the diameter of the PVC and a lip for the paper to tape onto. Then I extruded these circles to form a cap that will be the base of the nose cones.

Next I moved on to adding a hole that would allow less material to be used in printing the nose cones that would be above.  Then, I switched to a front view and then drew a 1/2 profile of the nose that would be rotated around the central axis of the nose. 

The only nose that deviated from the above was the flat square which required a loft as a new body, then used the shell tool to reduce printable material. Finally, I joined that nose with the base to form a single body. 



![Alt text](https://cdn.thingiverse.com/assets/71/c8/e9/6f/16/Fusion_cross_section_design.png)
Image of a possible nose cross section before revolution in Fusion 360

# Project: Paper Air Rocket Nose Cones

<iframe src="//www.youtube.com/embed/VPjsU4TNLrg" frameborder="0" allowfullscreen></iframe>

![Alt text](https://cdn.thingiverse.com/assets/74/57/a4/ba/55/20160828_134442.jpg)
Use PVC to line up nose and guide the rolling of the paper for the body

![Alt text](https://cdn.thingiverse.com/assets/42/ba/57/f0/53/20160828_134759.jpg)
Tape it all up and smooth it out

![Alt text](https://cdn.thingiverse.com/assets/5e/b5/28/99/a3/20160828_135839.jpg)
Launch setup from Make Magazine article

![Alt text](https://cdn.thingiverse.com/assets/be/7e/d8/3e/05/20160828_141748.jpg)
Try out lots of different ones (some of the tail sections from another project shown). The rocket on the right got bent up on a hard landing, but was ready for launch seconds later